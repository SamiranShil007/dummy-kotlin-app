package com.example.android.dummykotlinapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.ImageView
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide

class MainActivity : AppCompatActivity() {
    lateinit var adapter: PhotoItemAdapter
    lateinit var recyclerView: RecyclerView
    private val imageDataArray: ArrayList<ImageData> = ArrayList()
    private lateinit var circleImage: ImageView
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        recyclerView = findViewById(R.id.recyclerView)
        circleImage = findViewById(R.id.circleImage)
        Glide.with(circleImage.context).load("https://picsum.photos/id/20/200/200").circleCrop().into(circleImage)
        recyclerView.layoutManager = GridLayoutManager(this, 3)
        updateData()
        Log.d("Array Length", imageDataArray.toString())
        adapter = PhotoItemAdapter(imageDataArray)
        recyclerView.adapter = adapter
    }

    private fun updateData() {
        for (i in 0..19){
            imageDataArray.add(ImageData("https://picsum.photos/id/$i/200/200"))
        }
    }
}