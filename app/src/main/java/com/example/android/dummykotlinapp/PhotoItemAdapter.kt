package com.example.android.dummykotlinapp

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.android.dummykotlinapp.PhotoItemAdapter.PhotoItemViewHolder

class PhotoItemAdapter(private val items: ArrayList<ImageData>): RecyclerView.Adapter<PhotoItemAdapter.PhotoItemViewHolder>() {
    inner class PhotoItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val imageView: ImageView = itemView.findViewById(R.id.imageView)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PhotoItemViewHolder {
        val view: View = LayoutInflater.from(parent.context).inflate(R.layout.photo_items, parent, false)
        return PhotoItemViewHolder(view)
    }

    override fun onBindViewHolder(holder: PhotoItemViewHolder, position: Int) {
        val currentItem = items[position]
        Glide.with(holder.itemView.context).load(currentItem.imageUrl).into(holder.imageView)
    }

    override fun getItemCount(): Int {
        Log.d("Array Length", items.size.toString())
        return items.size
    }
}